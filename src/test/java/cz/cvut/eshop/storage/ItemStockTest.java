package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

public class ItemStockTest {

    @Test
    public void toStringTest() {
        StandardItem test_item = new StandardItem(1, "test", 1.f, "test", 1);
        ItemStock test_stock = new ItemStock(test_item);
        assertEquals("STOCK OF ITEM:  Item   ID 1   NAME test   CATEGORY test   PRICE 1.0   LOYALTY POINTS 1    PIECES IN STORAGE: 0",
                test_stock.toString());
    }

    @Test
    public void increaseItemCountTest() {
        ItemStock test_stock = new ItemStock(null);
        test_stock.IncreaseItemCount(1);
        assertEquals(1,test_stock.getCount());
    }

    @Test
    public void decreaseItemCountTest() {
        ItemStock test_stock = new ItemStock(null);
        test_stock.decreaseItemCount(1);
        assertEquals(-1,test_stock.getCount());
    }

    @Test
    public void getCountTest() {
        ItemStock test_stock = new ItemStock(null);
        assertEquals(0,test_stock.getCount());
    }

    @Test
    public void getItemTest() {
        StandardItem test_item = new StandardItem(1, "test", 1.f, "test", 1);
        ItemStock test_stock = new ItemStock(test_item);
        assertEquals(test_item,test_stock.getItem());
    }
}