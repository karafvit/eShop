package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.Order;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import static org.junit.Assert.*;

public class StorageTest {

    @Test
    public void getStockEntries() {
        Storage test_storage = new Storage();
        StandardItem test_item = new StandardItem(1, "test",1.f, "test", 1);
        test_storage.insertItems(test_item, 2);

        String expected_output = "[STOCK OF ITEM:  Item   ID 1   NAME test   CATEGORY test   PRICE 1.0   " +
                                    "LOYALTY POINTS 1    PIECES IN STORAGE: 2]";

        assertEquals(expected_output, test_storage.getStockEntries().toString());
    }

    @Test
    public void printListOfStoredItems() {
        Storage test_storage = new Storage();
        StandardItem test_item = new StandardItem(1, "test",1.f, "test", 1);
        test_storage.insertItems(test_item, 2);

        ByteArrayOutputStream test_out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(test_out));

        test_storage.printListOfStoredItems();

        String expected_output = "STORAGE IS CURRENTLY CONTAINING:\nSTOCK OF ITEM:  Item   ID 1   NAME test   " +
                                    "CATEGORY test   PRICE 1.0   LOYALTY POINTS 1    PIECES IN STORAGE: 2\n";

        assertEquals(expected_output, test_out.toString());
    }

    @Test
    public void removeItems_enough_items() {
        Storage test_storage = new Storage();
        StandardItem test_item = new StandardItem(1, "test",1.f, "test", 1);
        test_storage.insertItems(test_item, 2);

        try {
            test_storage.removeItems(test_item, 1);
        } catch (NoItemInStorage e)
        {
            e.printStackTrace();
        }

        ByteArrayOutputStream test_out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(test_out));

        test_storage.printListOfStoredItems();

        String expected_output = "STORAGE IS CURRENTLY CONTAINING:\nSTOCK OF ITEM:  Item   ID 1   NAME test   " +
                "CATEGORY test   PRICE 1.0   LOYALTY POINTS 1    PIECES IN STORAGE: 1\n";

        assertEquals(expected_output, test_out.toString());
    }

    @Test
    public void removeItems_no_items() {
        Storage test_storage = new Storage();
        StandardItem test_item = new StandardItem(1, "test",1.f, "test", 1);
        test_storage.insertItems(test_item, 2);

        try {
            test_storage.removeItems(test_item, 4);
        } catch (NoItemInStorage e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void removeItems_removing_too_much_items() {
        Storage test_storage = new Storage();
        StandardItem test_item = new StandardItem(1, "test",1.f, "test", 1);

        try {
            test_storage.removeItems(test_item, 1);
        } catch (NoItemInStorage e)
        {
            System.out.println(e);
        }
    }

    @Test
    public void processOrder() {
        Storage test_storage = new Storage();
        StandardItem test_item = new StandardItem(1, "test",1.f, "test", 1);

        test_storage.insertItems(test_item, 2);

        ShoppingCart test_shoShoppingCart = new ShoppingCart();
        test_shoShoppingCart.addItem(test_item);

        Order test_order = new Order(test_shoShoppingCart);


        try {
            test_storage.processOrder(test_order);
        } catch (NoItemInStorage e)
        {
            e.printStackTrace();
        }

        ByteArrayOutputStream test_out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(test_out));

        test_storage.printListOfStoredItems();

        String expected_output = "STORAGE IS CURRENTLY CONTAINING:\nSTOCK OF ITEM:  Item   ID 1   NAME test   " +
                "CATEGORY test   PRICE 1.0   LOYALTY POINTS 1    PIECES IN STORAGE: 1\n";

        assertEquals(expected_output, test_out.toString());

    }

    @Test
    public void getItemCount_item_in_storage() {
        Storage test_storage = new Storage();
        StandardItem test_item = new StandardItem(1, "test",1.f, "test", 1);

        test_storage.insertItems(test_item, 2);

        assertEquals(2, test_storage.getItemCount(test_item));
    }

    @Test
    public void getItemCount_item_not_in_storage() {
        Storage test_storage = new Storage();
        StandardItem test_item = new StandardItem(1, "test",1.f, "test", 1);

        assertEquals(0, test_storage.getItemCount(test_item));
    }

    @Test
    public void getItemCount1__item_in_storage() {
        Storage test_storage = new Storage();
        StandardItem test_item = new StandardItem(1, "test",1.f, "test", 1);

        test_storage.insertItems(test_item, 2);

        assertEquals(2, test_storage.getItemCount(test_item.getID()));
    }

    @Test
    public void getItemCount1__item_not_in_storage() {
        Storage test_storage = new Storage();
        StandardItem test_item = new StandardItem(1, "test",1.f, "test", 1);

        assertEquals(0, test_storage.getItemCount(test_item.getID()));
    }


    @Test
    public void getPriceOfWholeStock() {
        Storage test_storage = new Storage();
        StandardItem test_item = new StandardItem(1, "test",1.f, "test", 1);

        test_storage.insertItems(test_item, 2);

        //ERROR found in the function should be totalPrice += e.getItem().getPrice() * e.getCount();
        //Also going from float to int will lose some accuracy
        assertEquals(2, test_storage.getPriceOfWholeStock(), 0.00001);
    }

    @Test
    public void getItemsOfCategorySortedByPrice() {
        Storage test_storage = new Storage();
        StandardItem test_item1 = new StandardItem(1, "test1",1.f, "test", 1);
        StandardItem test_item2 = new StandardItem(2, "test2",2.f, "test", 2);

        test_storage.insertItems(test_item2, 2);
        test_storage.insertItems(test_item1, 2);

        Collection<Item> test_collection =  test_storage.getItemsOfCategorySortedByPrice("test");

        ByteArrayOutputStream test_out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(test_out));

        String expected_output = "Item   ID 1   NAME test1   CATEGORY test   PRICE 1.0   LOYALTY POINTS 1\n" +
                "Item   ID 2   NAME test2   CATEGORY test   PRICE 2.0   LOYALTY POINTS 2\n";

        /*
        sortItemsByPrice seems that it does not sort anything. HashMap doesn't have any guarantee
        that it will be sorted but here it seems it is keeping the Items sorted by itself.
        */
        for (Object aTest_collection : test_collection) {
            System.out.println(aTest_collection);
        }

        assertEquals(expected_output, test_out.toString());
    }
}